const glob = require('glob');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPugPlugin = require('html-webpack-pug-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const pages = glob.sync(__dirname + '/src/pages/*.pug')

const files = pages.map(file => {
  let base = path.basename(file, '.pug')

  return new HtmlWebpackPlugin({
    inject: true,
    filename: './' + base + '.html',
    template: './src/pages/' + base + '.pug'
  })
})

module.exports = {
  entry: ['./src/javascript/index.js', './src/styles/index.scss'],
  module: {
    rules: [
      { test: /\.(js)$/, use: 'babel-loader' },
      {
        test: /\.pug$/,
        loader: 'pug-loader'
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {}
          },
          {
            loader: 'css-loader',
            options: { sourceMap: true, url: false }
          },
          {
            loader: 'postcss-loader',
          },
          {
            loader: 'sass-loader',
            options: { sourceMap: true }
          }
        ],
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new CopyWebpackPlugin({
      patterns : [
        { from: './public/fonts', to: '../dist/fonts' },
        // { from: './public/favicon.ico', to: '../dist/favicon.ico' },
        { from: './public/images', to: '../dist/images' },
        { from: './src/php', to: '../dist/' }
      ]
    }),
    ...files,
    new HtmlWebpackPugPlugin({
      adjustIndent: true
    }),
  ],
  mode: "development",
  devServer: {
    contentBase: path.join(__dirname, 'src/'),
    compress: true,
    port: 9000,
    watchContentBase: true,
  }
}