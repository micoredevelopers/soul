$('a').click(function (e) {
  const link = $(this);
  const loader = $('.page-loader');
  const href = link.attr('href');
  const isBlank = link.attr('target');
  const hasDataModal = link.attr('data-modal');

  if(!isBlank && !hasDataModal) {
    e.preventDefault();
    setTimeout(function () {
      loader.addClass('show')
    }, 0)
    setTimeout(function () {
      loader.addClass('active')
    }, 0)
    setTimeout(function () {
      window.location.href = href;
    }, 1200)
  }
})
