import { setModal } from "./modal";

$(document).ready(function() {
  const slideBtn = $('.main-page-btn');
  const slides = $('.fp-section');
  const progressBar = $('.progress-bar');
  const pageContent = $('.main-page-content');
  const contentTitle = pageContent.find('.section-title');
  const titleSpan = contentTitle.find('span');
  const titleHeight = parseInt(titleSpan.css('height'));
  const contentDesc = pageContent.find('.section-desc');
  const descSpan = contentDesc.find('span');
  console.log(descSpan)
  const descHeight = parseInt(descSpan.css('height'));
  const isMobile = window.innerWidth < 993;
  const buttonData = [
    {
      href: '/cases.html',
      text: 'Смотреть кейсы',
    },
    {
      href: '/services.html',
      text: 'Смотреть услуги',
    },
    {
      href: '#',
      text: 'Смотреть пример отчёта',
    },
    {
      text: 'Получить консультацию',
      modal: '#consultation-modal',
    }
  ]

  contentTitle.find('.container-mask').css('height', `${titleHeight}px`);
  titleSpan.css('marginBottom', `${titleHeight}px`)
  titleSpan.css('height', `${titleHeight}px`)
  contentDesc.find('.container-mask').css('height', `${descHeight}px`);
  descSpan.css('marginBottom', `${descHeight}px`)
  descSpan.css('height', `${descHeight}px`)

  progressBar.find('.progress').css('width', `${1 / slides.length * 100}%`)

  function setHeight(items, height, itemIndex) {
    $(items).each(function (index, item) {
      const elem = $(item);
      const spans = $(elem.find('span'));

      spans.each(function (index, item) {
        const elem = $(item);

        if(itemIndex === index) {
          if(elem.text() === '') {
            elem.parents('.container-mask').css({'height': '0px'})
          } else {
            elem.parents('.container-mask').css({'height': `${height}px`})
          }
        }
      })
    })
  }

  $('#fullpage').fullpage({
    scrollHorizontally: false,
    scrollingSpeed: 2000,
    dragAndMove: isMobile,
    onLeave: function(origin, destination, direction){
      const leavingSection = $(this.item);
      pageContent.removeClass('down up')
      pageContent.addClass(`animation-start ${direction}`);
      $('#fullpage').addClass(`animation-start ${direction}`);
      $('.fp-section').removeClass('setup-animation');
      slideBtn.removeAttr('data-modal');
      slideBtn.removeAttr('href');

      switch (direction) {
        case 'down':
          const currentSlideBtnDown = buttonData[origin.index + 1];
          contentTitle.attr('data-current', `${origin.index + 2}`)
          setHeight('.section-desc-mask-container', descHeight, origin.index + 1)
          setHeight('.section-title-mask-container', titleHeight, origin.index + 1)
          contentTitle.find('.section-title-mask-container').css('transform', `translateY(-${(origin.index + 1) * titleHeight * 2 }px)`);
          contentDesc.find('.section-desc-mask-container').css('transform', `translateY(-${(origin.index + 1) * descHeight * 2 }px)`);
          progressBar.find('.progress').css('width', `${(origin.index + 2) / slides.length * 100}%`)
          slideBtn.find('span').html(currentSlideBtnDown.text);
          if (currentSlideBtnDown.href) {
            slideBtn.attr('href', currentSlideBtnDown.href);
          }
          if (currentSlideBtnDown.modal) {
            slideBtn.attr('data-modal', currentSlideBtnDown.modal);
            setModal(currentSlideBtnDown.modal)
          }
          leavingSection.addClass('down')
          leavingSection.removeClass('up')
          break;
        case 'up':
          const currentSlideBtnUp = buttonData[origin.index - 1];
          contentTitle.attr('data-current', `${origin.index }`)
          setHeight('.section-desc-mask-container', descHeight, origin.index - 1)
          setHeight('.section-title-mask-container', titleHeight, origin.index - 1)
          contentTitle.find('.section-title-mask-container').css('transform', `translateY(-${(origin.index - 1) * titleHeight * 2 }px)`);
          contentDesc.find('.section-desc-mask-container').css('transform', `translateY(-${(origin.index - 1) * descHeight * 2 }px)`);
          progressBar.find('.progress').css('width', `${(origin.index) / slides.length * 100}%`);
          slideBtn.find('span').html(currentSlideBtnUp.text);
          if (currentSlideBtnUp.href) {
            slideBtn.attr('href', currentSlideBtnUp.href);
          }
          if (currentSlideBtnUp.modal) {
            slideBtn.attr('data-modal', currentSlideBtnUp.modal);
            setModal(currentSlideBtnUp.modal)
          }
          leavingSection.addClass('up')
          leavingSection.removeClass('down')
          break;
      }
    },
    afterLoad: function (origin, destination, direction) {
      const section = this;
      setTimeout(function () {
        $('.fp-section').addClass('setup-animation');
      }, 0)
      setTimeout(function () {
        $('.fp-section').removeClass('animation-start ');
        setTimeout(function () {
          $('.fp-section').removeClass('setup-animation');
        }, 0)
      }, 0)
    }
  });
});

function setHorizontalScroll(selector, wrapper, item) {
  const elem = document.querySelector(selector);

  if (elem.addEventListener) {
    if ('onwheel' in document) {
      // IE9+, FF17+, Ch31+
      elem.addEventListener("wheel", onWheel);
    } else if ('onmousewheel' in document) {
      // устаревший вариант события
      elem.addEventListener("mousewheel", onWheel);
    } else {
      // Firefox < 17
      elem.addEventListener("MozMousePixelScroll", onWheel);
    }
  } else { // IE8-
    elem.attachEvent("onmousewheel", onWheel);
  }

  let translateX = 0;

  function getTranslateX() {
    const style = window.getComputedStyle(document.querySelector(wrapper));
    const matrix = new WebKitCSSMatrix(style.transform);
   return matrix.m41;
  }


  function onWheel(event) {
    const e = event || window.event;
    const caseItem = $(item);
    const container = $(wrapper);
    translateX =  getTranslateX();
    // wheelDelta не даёт возможность узнать количество пикселей
    const delta = e.deltaY || e.detail || e.wheelDelta;

    const width = caseItem.length * caseItem.width() + parseInt(caseItem.css('marginRight'), 10) * caseItem.length - 1;
    translateX -= delta;
    const maxWidthScroll = -width + window.innerWidth - 170;

    if(window.innerWidth < width) {
      if (translateX < maxWidthScroll) {
        translateX = maxWidthScroll;
      } else if (translateX > 0) {
        translateX = 0;
      }
      container.css('transform', `translateX(${translateX}px)`);
    }

    e.preventDefault ? e.preventDefault() : (e.returnValue = false);
  }
}

if(window.innerWidth > 992) {
  const id = $('main').attr('id');

  switch (id) {
    case 'services-page':
      setHorizontalScroll('#services-page-slider', '.services-container', '.service')
      break;
    case 'cases-page':
      setHorizontalScroll('#cases-page-slider', '.cases-container', '.case.active')
      break;
    case 'clients-page':
      setHorizontalScroll('#clients-page-slider', '.clients-container', '.client')
      break;
  }
}
