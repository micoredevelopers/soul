function handleClick(e) {
  const menu = $(this);
  const menuWrap = $('.menu');
  menu.off("click")
  menu.toggleClass('active');
  if(!menu.hasClass('backward')) {
    setTimeout(() => {
      menu.addClass('backward')
      menuWrap.addClass('backward')
    }, 0)
  } else {
    setTimeout(() => {
      menu.removeClass('backward')
      menuWrap.removeClass('backward')
    }, 1000)
  }
  setTimeout(() => {
    menuWrap.toggleClass('opened');
    $('body').toggleClass('menu-opened');
  })

  setTimeout(function () {
    menu.on('click', handleClick)
  }, 1000)
}

$('.menu .menu-burger').on('click', handleClick)