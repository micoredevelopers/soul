function setGlider(slider, container, item) {
  const elem = document.querySelector(slider);
  const wrapper = document.querySelector(container);
  const caseItems = $(wrapper).find(item);

  elem.onmousedown = function(e) {
    function getTranslateX() {
      const style = window.getComputedStyle(wrapper);
      const matrix = new WebKitCSSMatrix(style.transform);
      return matrix.m41;
    }

    let translate = getTranslateX();
    let shiftX = e.pageX - translate;

    const width = caseItems.length * caseItems.width() + parseInt(caseItems.css('marginRight'), 10) * caseItems.length - 1;
    const maxWidthScroll = -width + window.innerWidth - 170;

    function moveAt(e) {
      translate = getTranslateX();
      translate = (e.pageX - shiftX);

      if (translate < maxWidthScroll) {
        translate = maxWidthScroll;
      } else if (translate > 0) {
        translate = 0;
      }

      wrapper.style.transform = `translateX(${translate + 'px'})`;
    }

    document.onmousemove = function(e) {
      if(width > window.innerWidth) {
        moveAt(e);
      }
    };

    elem.onmouseup = function() {
      document.onmousemove = null;
      elem.onmouseup = null;
    };

  }

  elem.ondragstart = function() {
    return false;
  };
}

const id = $('main').attr('id');

switch (id) {
  case 'rakhim-page':
    setGlider('.songs-slider','#rakhim-page .songs-container', '.song')
    break;
}