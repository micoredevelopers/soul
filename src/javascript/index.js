import "./svg";
import "./menu";
import "./modal";
import "./form";
import "./cases";
import "./slider";
import "./marquee";
import "./glider";
import "./pageLoader";
import "./animation";
import "./ajaxform";
