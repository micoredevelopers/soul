export function setModal(id) {
  const modal = $(id);

  $(`[data-modal="${id}"]`).on('click', function (e) {
    modal.addClass('active');
  })

  modal.find('.close').on('click', function (e) {
    modal.removeClass('active')
  })

  modal.find('.close-btn').on('click', function (e) {
    modal.removeClass('active')
  })
}

setModal('#mailing-modal');
setModal('#consultation-modal');
setModal('#thanksgiving');
setModal('#errorModal');