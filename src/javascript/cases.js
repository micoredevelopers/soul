function setSlides() {
  const cases = $('.case');
  cases.addClass('active');

  $('.tab').on('click', function () {
    const tab = $(this);
    $('.cases-container').css('transform', 'translateX(0)')

    $('.tab').removeClass('active');
    tab.addClass('active');
    const tabType = tab.text();

    setTimeout(() => {
      cases.removeClass('active');
    }, 0)

    setTimeout(() => {
      if(tabType === 'Все') {
        cases.addClass('active');
      } else {
        cases.removeClass('active');
        $(`.case[data-type="${tabType}"]`).addClass('active');
      }
      $(cases[cases.length - 1]).addClass('active');
    }, 100);
  })
}

setSlides();
