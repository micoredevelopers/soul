$(document).ready(function() { // вся магия после загрузки страницы
	setConsultForm('#contacts-form',['name', 'phone', 'email']);
	setConsultForm('.case-form #consultation-form', ['name', 'phone']);
	setConsultForm('#consultation-modal-form', ['name', 'phone', 'email']);
	
	$("#subscribe-form").submit(function(e){ // перехватываем все при событии отправки
		const form = $(this); // запишем форму, чтобы потом не было проблем с this
		const noErrors = validate(['email'], form);
		if (noErrors) { // если ошибки нет
			const data = form.serialize(); // подготавливаем данные
			$.ajax({ // инициализируем ajax запрос
			   type: 'POST', // отправляем в POST формате, можно GET
			   url: 'sendmail2.php', // путь до обработчика, у нас он лежит в той же папке
			   dataType: 'json', // ответ ждем в json формате
			   data: data, // данные для отправки
		       beforeSend: function(data) { // событие до отправки
		            form.find('#sendmail2').attr('disabled', 'disabled'); // например, отключим кнопку, чтобы не жали по 100 раз
		          },
		       success: function(data){ // событие после удачного обращения к серверу и получения ответа
						 if (data['error']) { // если обработчик вернул ошибку
							 $('#errorModal').addClass('active');
							 console.log('err');
						 } else { // если все прошло ок
							 $('#thanksgiving').addClass('active');
							 form.trigger('reset');
						 }
		         },
		       error: function (xhr, ajaxOptions, thrownError) { // в случае неудачного завершения запроса к серверу
						 	$('#errorModal').addClass('active');
						 console.log('err');
					 },
		       complete: function(data) { // событие после любого исхода
		            form.find('#sendmail2').prop('disabled', false); // в любом случае включим кнопку обратно
					 }
		                  
			     });
		}
		return false; // вырубаем стандартную отправку формы
	});
});
function setConsultForm(id, validatingFields) {
	$(id).submit(function(e){
		console.log('form');// перехватываем все при событии отправки
		const form = $(this); // запишем форму, чтобы потом не было проблем с this
		const noErrors = validate(validatingFields, form);
		console.log(noErrors);
		if (noErrors) { // если ошибки нет
			const data = form.serialize(); // подготавливаем данные
			$.ajax({ // инициализируем ajax запрос
				type: 'POST', // отправляем в POST формате, можно GET
				url: 'sendmail.php', // путь до обработчика, у нас он лежит в той же папке
				dataType: 'json', // ответ ждем в json формате
				data: data, // данные для отправки
				beforeSend: function(data) { // событие до отправки
					form.find('#sendmail').attr('disabled', 'disabled'); // например, отключим кнопку, чтобы не жали по 100 раз
				},
				success: function(data){ // событие после удачного обращения к серверу и получения ответа
					if (data['error']) { // если обработчик вернул ошибку
						$('#errorModal').addClass('active');
						$('#consultation-modal').removeClass('active');
					} else { // если все прошло ок
						$('#consultation-modal').removeClass('active');
						$('#thanksgiving').addClass('active');
						form.trigger('reset');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) { // в случае неудачного завершения запроса к серверу
					$('#errorModal').addClass('active');
				},
				complete: function(data) { // событие после любого исхода
					form.find('#sendmail').prop('disabled', false); // в любом случае включим кнопку обратно
				}
			});
		}
		return false; // вырубаем стандартную отправку формы
	});
}

function validate(inputs = [], form) {
	$('.input-wrap').attr('data-aos', 'false')
	$('.error-hint').remove();
	form.find('input').removeClass('error');

	return inputs.every(item => {
		const input = $(form.find(`input[name="${item}"]`));
		if(!input.val()) {
			input.addClass('error');
			input.after(function () {
				return `<div class="error-hint">Поле не должно быть пустым</div>`
			})
			return false
		} else if (input.attr('name') === 'email') {
			const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (!re.test(String(input.val()).toLowerCase())) {
				input.addClass('error');
				input.after(function () {
					return `<div class="error-hint">Проверьте правильность написания email</div>`
				})
				return false;
			} else {
				return true
			}
		} else {
			return true;
		}
	})
}