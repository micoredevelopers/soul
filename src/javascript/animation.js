$(document).ready(() => {
  AOS.init({
    startEvent: 'DOMContentLoaded',
    easing: 'cubic-bezier(0.4, 0, 0.2, 1)',
    duration: 1000,
    once: true,
    offset: 0
  });
})

function refreshAos () {
  if(window.innerWidth > 100) {
    AOS.refresh({
      startEvent: 'DOMContentLoaded',
      easing: 'cubic-bezier(0.4, 0, 0.2, 1)',
      duration: 1000,
      once: true,
      offset: 0
    });
    console.log('ss');
    $(document).off('scroll', refreshAos)
  }
}

$(document).on('scroll', refreshAos)